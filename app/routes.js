var express 	= require('express'); // load express package.
var path 		= require('path'); // load path package.
var router 		= express.Router(); // set router to express router function.
module.exports	= router; // export router for global use.
var pagesRouted; // variable for router loop.

function routePages() {
	// route for our homepage
	router.get('/', function(req, res) {
		res.render('pages/home');
	});

	// route for our about page (with a bunch of fake users in a object array)
	router.get('/about', function(req, res) {
		var users = [
			{ name: 'Holly', email:'holly@scotch.io', avatar: 'http://www.howtogeek.com/wp-content/uploads/2013/08/650x300xdog-guest-browsing.jpg.pagespeed.gp+jp+jw+pj+js+rj+rp+rw+ri+cp+md.ic.4RfhAPp9z-.jpg' },
			{ name: 'Steve', email:'steve@scotch.io', avatar: 'https://www.creativecali.com/wp-content/uploads/browsing-the-internets.jpg' },
			{ name: 'Mark', email:'mark@scotch.io', avatar: 'http://i1.wp.com/wildammo.com/wp-content/uploads/2012/03/cute-animals-using-computers-wildammo-t-3.jpg?w=600' },
			{ name: 'Jeff', email:'jeff@scotch.io', avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQP8Wj00B-etot3CFzdUuV3fvqJBllb9EBJnch0haMRO1g9NTUf8A' }
		];

		res.render('pages/about', { users: users });
	});

	// route for the contact page, get and post because we want there to be a form on this page
	router.get('/contact', function(req, res) {
		res.render('pages/contact');
	});

	router.post('/contact', function(req, res) {
		res.send('Thanks for contacting us, ' + req.body.name + ' ! We will respond shorty!');
	});;
};

// Main router loop, if pagesRouter is undefined set it to false.
// For all other cases start the router and set variable to true.
if(pagesRouted === 'undefined') {
	pagesRouted = false;
}
else {
	routePages();
	pagesRouted = true;
}
