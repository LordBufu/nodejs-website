var express = require('express'); // load express package
var expressLayouts = require('express-ejs-layouts'); // load express esj layouts package
var bodyParser = require('body-parser'); // load body parser package
var app = express(); // set app to express
var running; // variable for the server loop

function server() {
	// router and port variables
	var router = require('./app/routes');
	var port = 8080;
	
	// use ejs and express for layout support
	app.set('view engine', 'ejs');
	app.use(expressLayouts);

	// use body parser
	// needs the extended tag, the course i did dint cover that and it caused an error.
	app.use(bodyParser.urlencoded({ extended: false }));

	// route app
	app.use('/', router);
	app.use(express.static(__dirname + '/public'));

	// start server
	app.listen(port, function() {
		console.log('app started');
	});
};


// Main server loop, if variable is undefined set it to false
// for all other cases start the server, and set variable to true
if(running === 'undefined') {
	// set to false
	running = false;
}
else {
	server();
	running = true;
}
